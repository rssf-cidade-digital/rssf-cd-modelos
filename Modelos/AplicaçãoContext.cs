﻿using Modelos.Modelos;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Modelos
{
    public class AplicaçãoContext : DbContext
    {
        public DbSet<CélulaModelo> Células { get; set; }
        public DbSet<AlertaModelo> Alertas { get; set; }
        public DbSet<RequisiçãoModelo> Requisições { get; set; }
        public DbSet<MonitoramentoModelo> Monitoramentos { get; set; }
        public DbSet<UsuárioModelo> Usuários { get; set; }
        public DbSet<RequisiçãoAtendidaModelo> RequisiçõesAtendidas { get; set; }
        public DbSet<NívelAlertaModelo> NíveisAlerta { get; set; }
        public DbSet<NívelUsuárioModelo> NíveisUsuários { get; set; }
        public DbSet<PessoaAvisarModelo> PessoasAvisar { get; set; }
        public DbSet<PessoaAvisadaModelo> PessoasAvisadas { get; set; }

        public AplicaçãoContext(DbContextOptions<AplicaçãoContext> options) : base(options)
        {

        }

        public AplicaçãoContext()
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<NívelAlertaModelo>().HasKey(n => n.Id);

            modelBuilder.Entity<NívelUsuárioModelo>().HasKey(n => n.Id);

            modelBuilder.Entity<AlertaModelo>().HasKey(a => a.Id);
            modelBuilder.Entity<AlertaModelo>().HasOne(a => a.Monitoramento);
            modelBuilder.Entity<AlertaModelo>().HasOne(a => a.Nível);

            modelBuilder.Entity<CélulaModelo>().HasKey(c => c.Id);

            modelBuilder.Entity<MonitoramentoModelo>().HasKey(m => m.Id);
            modelBuilder.Entity<MonitoramentoModelo>().HasOne(m => m.Célula);
            modelBuilder.Entity<MonitoramentoModelo>().HasOne(m => m.Requisição);

            modelBuilder.Entity<RequisiçãoAtendidaModelo>().HasKey(r => r.Id);
            modelBuilder.Entity<RequisiçãoAtendidaModelo>().HasOne(r => r.Célula);
            modelBuilder.Entity<RequisiçãoAtendidaModelo>().HasOne(r => r.Requisição);

            modelBuilder.Entity<RequisiçãoModelo>().HasKey(r => r.Id);

            modelBuilder.Entity<UsuárioModelo>().HasKey(u => u.Id);
            modelBuilder.Entity<UsuárioModelo>().HasOne(u => u.Nível);

            modelBuilder.Entity<PessoaAvisarModelo>().HasKey(p => p.Id);
            modelBuilder.Entity<PessoaAvisarModelo>().HasIndex(p => p.IdChatTelegram).IsUnique();
            modelBuilder.Entity<PessoaAvisarModelo>().HasOne(p => p.Célula);

            modelBuilder.Entity<PessoaAvisadaModelo>().HasKey(p => p.Id);
            modelBuilder.Entity<PessoaAvisadaModelo>().HasOne(p => p.Alerta);
            modelBuilder.Entity<PessoaAvisadaModelo>().HasOne(p => p.Pessoa);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

#if EF
            optionsBuilder.UseMySql("Server=BDIoT;User Id=admin.rssf;Password=RsSf-123456*;Database=rssf", b => b.MigrationsAssembly("APICelulas"));
#else

    #if DEBUG
            var alvoAmbiente = EnvironmentVariableTarget.User;
    #endif

    #if DOCKER
            var alvoAmbiente = EnvironmentVariableTarget.Process;
    #endif
    #if RELEASE
            var servidor = "170.246.105.251";
            var usuário = "admin.rssf";
            var senha = "RsSf-123456*";
            var baseDeDados = "rssf";
    #else
            var servidor = Environment.GetEnvironmentVariable("ServidorMySQL", alvoAmbiente);
            var usuário = Environment.GetEnvironmentVariable("UsuarioMySQL", alvoAmbiente);
            var senha = Environment.GetEnvironmentVariable("SenhaMySQL", alvoAmbiente);
            var baseDeDados = Environment.GetEnvironmentVariable("BaseDeDadosMySQL", alvoAmbiente);
    #endif

            optionsBuilder.UseMySql($"Server={servidor};User Id={usuário};Password={senha};Database={baseDeDados}");
#endif
        }
    }
}
