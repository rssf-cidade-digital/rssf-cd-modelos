﻿using Modelos.Interfaces;
using Modelos.Modelos;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Modelos.Bases;

namespace Modelos.Repositórios
{
    public class RequisiçõesAtendidasRepositório : BaseRequisiçõesAtendidasRepositório
    {
        public RequisiçõesAtendidasRepositório(AplicaçãoContext contexto) : base(contexto)
        {
        }

        public override List<RequisiçãoAtendidaModelo> RetornarLista(Expression<Func<RequisiçãoAtendidaModelo, bool>> filtro = null)
        {
            IOrderedQueryable<RequisiçãoAtendidaModelo> linqRetorno;
            if (filtro == null)
            {
                linqRetorno = _baseDeDados.Include(r => r.Requisição).OrderBy(r => r.Id);
            }
            else
            {
                linqRetorno = _baseDeDados.Where(filtro).Include(r => r.Requisição).OrderBy(r => r.Id);
            }

            var lista = linqRetorno.ToList();
            lista.Reverse();

            return lista;
        }
    }
}
