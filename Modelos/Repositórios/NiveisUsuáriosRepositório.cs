﻿using System;
using Modelos.Bases;
using Modelos.Interfaces;
using Modelos.Modelos;

namespace Modelos.Repositórios
{
    public class NíveisUsuáriosRepositório : BaseNíveisUsuáriosRepositório
    {
        public NíveisUsuáriosRepositório(AplicaçãoContext contexto) : base(contexto)
        {
        }
    }
}
