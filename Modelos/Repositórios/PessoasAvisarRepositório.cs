﻿using Microsoft.EntityFrameworkCore;
using Modelos.Bases;
using Modelos.Interfaces;
using Modelos.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Modelos.Repositórios
{
    public class PessoasAvisarRepositório : BasePessoasAvisarRepositório
    {
        public PessoasAvisarRepositório(AplicaçãoContext contexto) : base(contexto)
        {
        }

        public override List<PessoaAvisarModelo> RetornarLista(Expression<Func<PessoaAvisarModelo, bool>> filtro = null)
        {
            if(filtro == null)
            {
                return _baseDeDados.Include(p => p.Célula).ToList();
            }

            return _baseDeDados.Where(filtro).Include(p => p.Célula).ToList();
        }
    }
}
