﻿using Microsoft.EntityFrameworkCore;
using Modelos.Bases;
using Modelos.Interfaces;
using Modelos.Modelos;
using Modelos.ModelosExibição;
using Modelos.Serviços;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Modelos.Repositórios
{
    public class UsuáriosRepositório : BaseUsuáriosRepositório
    {
        public UsuáriosRepositório(AplicaçãoContext contexto) : base(contexto)
        {
        }

        public override UsuárioModeloExibição Logar(LoginModeloExibição login)
        {
            var senhaCodificada = CriptografiaServiço.SHA1(login.Senha);

            var resultado = RetornarLista(u => u.Usuário == login.NomeUsuário && u.Senha == senhaCodificada);

            if(resultado.Count != 1)
            {
                return null;
            }

            return resultado.Single().RetornarModeloExibição();  
        }

        public override UsuárioModelo RetornarPeloId(int id)
        {
            var usuário =  _baseDeDados.Where(u => u.Id == id).Include(u => u.Nível).SingleOrDefault();
            if(usuário == null)
            {
                return null;
            }
            usuário.Senha = "";

            return usuário;
        }

        public override List<UsuárioModelo> RetornarLista(Expression<Func<UsuárioModelo, bool>> filtro = null)
        {
            List<UsuárioModelo> usuários;
            if(filtro == null)
            {
                usuários = _baseDeDados.Include(u => u.Nível).ToList();
            }
            else
            {
                usuários = _baseDeDados.Where(filtro).Include(u => u.Nível).ToList();
            }
            foreach (var item in usuários)
            {
                item.Senha = "";
            }

            return usuários;
        }

        public override bool Modificar(int id, UsuárioModelo registro)
        {
            if(String.IsNullOrEmpty(registro.Senha))
            {
                registro.Senha = base.RetornarPeloId(id).Senha;
            }
            else
            {
                registro.Senha = CriptografiaServiço.SHA1(registro.Senha);
            }

            return base.Modificar(id, registro);
        }

        protected override void InserirItem(UsuárioModelo item)
        {
            item.Senha = CriptografiaServiço.SHA1(item.Senha);
            base.InserirItem(item);
        }
    }
}
