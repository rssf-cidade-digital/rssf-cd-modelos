﻿using Modelos.Bases;
using Modelos.Interfaces;
using Modelos.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Modelos.Repositórios
{
    public class RequisiçõesRepositório : BaseRequisiçõesRepositório
    {
        public RequisiçõesRepositório(AplicaçãoContext contexto) : base(contexto)
        {
        }

        public override List<RequisiçãoModelo> RetornarLista(Expression<Func<RequisiçãoModelo, bool>> filtro = null)
        {
            IOrderedQueryable<RequisiçãoModelo> linqRetorno;
            if (filtro == null)
            {
                linqRetorno = _baseDeDados.OrderBy(r => r.Id);
            }
            else
            {
                linqRetorno = _baseDeDados.Where(filtro).OrderBy(r => r.Id);
            }

            var lista = linqRetorno.ToList();
            lista.Reverse();

            return lista;

        }
    }
}
