﻿using Modelos.Bases;
using Modelos.Interfaces;
using Modelos.Modelos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Modelos.Repositórios
{
    public class PessoasAvisadasRepositório : BasePessoasAvisadasRepositório
    {
        public PessoasAvisadasRepositório(AplicaçãoContext contexto) : base(contexto)
        {
        }
    }
}
