﻿using Modelos.Bases;
using Modelos.Interfaces;
using Modelos.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Modelos.Repositórios
{
    public class AlertasRepositório : BaseAlertasRepositório

    {
        public AlertasRepositório(AplicaçãoContext contexto) : base(contexto)
        {
        }
    }
}
