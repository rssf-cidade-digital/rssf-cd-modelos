﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Modelos.Serviços
{
    public static class CriptografiaServiço
    {
        public static string SHA1(string dado)
        {
            if(String.IsNullOrEmpty(dado))
            {
                return "";
            }

            var criptografia = new SHA1CryptoServiceProvider();
            var bytesDado = Encoding.UTF8.GetBytes(dado);
            var bytesCripto = criptografia.ComputeHash(bytesDado);
            var stringCripto = BitConverter.ToString(bytesCripto).Replace("-", "").ToLower();

            return stringCripto;
        }
    }
}
