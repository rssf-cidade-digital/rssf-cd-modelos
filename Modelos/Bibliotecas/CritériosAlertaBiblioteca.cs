﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Modelos.Bibliotecas
{
    public static class CritériosAlertaBiblioteca
    {
        public static bool ChecarRadiaçãoUV(double radiaçãoUV)
        {
            return radiaçãoUV > 4;
        }

        public static bool ChecarIncêndio(double incêndio)
        {
            return incêndio == 1.0;
        }

        public static bool ChecarNívelGás(double gásTóxico)
        {
            return gásTóxico >= 900;
        }

        public static bool ChecarUmidade(double umidade)
        {
            return umidade <= 30;
        }
        public static bool ChecarTemperatura(double temperatura)
        {
            return temperatura >= 35;
        }
    }
}
