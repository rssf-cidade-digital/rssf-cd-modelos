﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Modelos.ModelosExibição
{
    public class LoginModeloExibição
    {
        public string NomeUsuário { get; set; }
        public string Senha { get; set; }
    }
}
