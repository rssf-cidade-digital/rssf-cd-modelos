﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Modelos.Modelos;

namespace Modelos.ModelosExibição
{
    public class UsuárioModeloExibição
    {
        public string Nome { get; set; }
        public NívelUsuárioModelo Nível { get; set; }

        public UsuárioModeloExibição(string nome, NívelUsuárioModelo nível)
        {
            Nome = nome;
            Nível = nível;
        }

    }
}
