﻿using Modelos.Bases;
using Modelos.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Modelos.Interfaces
{
    public interface IBaseRepositório<T> where T: BaseModelo
    {
        public T RetornarPeloId(int id);
        public T RetornarÚnico(Expression<Func<T, bool>> filtro);
        public T RetornarÚltimo(Expression<Func<T, bool>> filtro = null, bool ordernarPeloId = false);
        public List<T> RetornarLista(Expression<Func<T, bool>> filtro = null);
        public bool InserirVários(List<T> itens);
        public bool InserirUm(T item);
        public bool Modificar(int id, T registro);
        public bool Remover(int id);
    }
}
