﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Modelos.Bases;

namespace Modelos.Modelos
{
    public class MonitoramentoModelo : BaseModelo
    {
        [Required]
        public CélulaModelo Célula { get; set; }
        [Required]
        public RequisiçãoModelo Requisição { get; set; }
        [Required]
        public double Temperatura { get; set; }
        [Required]
        public double Umidade { get; set; }
        [Required]
        public double GásTóxico { get; set; }
        [Required]
        public double Fumaça { get; set; }
        [Required]
        public double Incêndio { get; set; }
        [Required]
        public double RadiaçãoUV { get; set; }
        [Required]
        public DateTime DataHora { get; set; }

        public MonitoramentoModelo()
        {

        }

        public MonitoramentoModelo(CélulaModelo célula, RequisiçãoModelo requisição, double temperatura, 
            double umidade, double gásTóxico, double fumaça, double incêndio, double radiaçãoUV)
        {
            Célula = célula;
            Requisição = requisição;
            Temperatura = temperatura;
            Umidade = umidade;
            GásTóxico = gásTóxico;
            Fumaça = fumaça;
            Incêndio = incêndio;
            RadiaçãoUV = radiaçãoUV;
        }

        public override void Atualizar(BaseModelo item)
        {
            throw new NotImplementedException();
        }
    }
}
