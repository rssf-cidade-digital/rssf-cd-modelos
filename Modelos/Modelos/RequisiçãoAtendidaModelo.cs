﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Modelos.Bases;

namespace Modelos.Modelos
{
    public class RequisiçãoAtendidaModelo : BaseModelo
    {
        public CélulaModelo Célula { get; set; }
        public RequisiçãoModelo Requisição { get; set; }
        public override void Atualizar(BaseModelo item)
        {
            throw new NotImplementedException();
        }
    }
}
