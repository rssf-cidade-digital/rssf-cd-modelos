﻿using Modelos.Bases;
using Modelos.Interfaces;
using Modelos.ModelosExibição;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Modelos.Modelos
{
    public class UsuárioModelo : BaseModelo, ITemModeloExibição<UsuárioModeloExibição>
    {
        [Required]
        public string Nome { get; set; }
        [Required]
        public string Usuário { get; set; }
        [Required]
        public string Senha { get; set; }
        [Required]
        public NívelUsuárioModelo Nível { get; set; }
        public UsuárioModelo()
        {

        }

        public UsuárioModelo(string nome, string usuário, string senha, NívelUsuárioModelo nível)
        {
            Nome = nome;
            Usuário = usuário;
            Senha = senha;
            Nível = nível;
        }

        public UsuárioModeloExibição RetornarModeloExibição()
        {
            return new UsuárioModeloExibição(Nome, Nível);
        }

        public override void Atualizar(BaseModelo item)
        {
            var usuário = item as UsuárioModelo;

            if (!(String.IsNullOrEmpty(usuário.Nome)))
            {
                Nome = usuário.Nome;
            }

            if (!(String.IsNullOrEmpty(usuário.Usuário)))
            {
                Usuário = usuário.Usuário;
            }

            if (!(String.IsNullOrEmpty(usuário.Senha)))
            {
                Senha = usuário.Senha;
            }

            Nível = usuário.Nível;

        }
    }
}
