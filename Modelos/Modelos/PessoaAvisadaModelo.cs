﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Modelos.Bases;

namespace Modelos.Modelos
{
    public class PessoaAvisadaModelo : BaseModelo
    {
        [Required]
        public AlertaModelo Alerta { get; set; }
        [Required]
        public PessoaAvisarModelo Pessoa { get; set; }

        public PessoaAvisadaModelo()
        {

        }

        public PessoaAvisadaModelo(AlertaModelo alerta, PessoaAvisarModelo pessoa)
        {
            Alerta = alerta;
            Pessoa = pessoa;
        }
        public override void Atualizar(BaseModelo item)
        {
            throw new NotImplementedException();
        }
    }
}
