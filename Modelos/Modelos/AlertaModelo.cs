﻿using System;
using System.ComponentModel.DataAnnotations;
using Modelos.Bases;

namespace Modelos.Modelos
{
    public class AlertaModelo : BaseModelo
    {
        [Required]
        public MonitoramentoModelo Monitoramento { get; set; }
        [Required]
        public NívelAlertaModelo Nível {get; set; }
        [Required]
        public string MensagemAlerta { get; set; }

        public AlertaModelo()
        {

        }

        public AlertaModelo(MonitoramentoModelo monitoramento, NívelAlertaModelo nível, string mensagemAlerta)
        {
            Monitoramento = monitoramento;
            Nível = nível;
            MensagemAlerta = mensagemAlerta;
        }

        public override void Atualizar(BaseModelo item)
        {
            var alerta = item as AlertaModelo;

            if (!(alerta.Monitoramento == null))
            {
                Monitoramento = alerta.Monitoramento;
            }

            if (!String.IsNullOrEmpty(alerta.MensagemAlerta))
            {
                MensagemAlerta = alerta.MensagemAlerta;
            }
        }
    }
}
