using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Modelos.Bases;

namespace Modelos.Modelos
{
    public class NívelUsuárioModelo : BaseModelo
    {
        [Required]
        public string Descrição { get; set; }
        [Required]
        public int Valor { get; set; }

        public override void Atualizar(BaseModelo item) => throw new NotImplementedException();
    }
}