﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Modelos.Bases;

namespace Modelos.Modelos
{
    public class RequisiçãoModelo : BaseModelo
    {
        [Required]
        public DateTime Momento { get; set; }

        public RequisiçãoModelo()
        {

        }

        public RequisiçãoModelo(DateTime momento, bool atendida)
        {
            Momento = momento;
        }

        public override void Atualizar(BaseModelo item)
        {
            throw new NotImplementedException();
        }
    }
}
