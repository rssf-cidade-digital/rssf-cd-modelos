using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Modelos.Bases;

namespace Modelos.Modelos
{
    public class PessoaAvisarModelo : BaseModelo
    {
        [Required]
        public CélulaModelo Célula { get; set; }
        [Required]
        public Int64 IdChatTelegram { get; set; }

        public PessoaAvisarModelo()
        {

        }

        public PessoaAvisarModelo(CélulaModelo célula, long idChatTelegram)
        {
            Célula = célula;
            IdChatTelegram = idChatTelegram;
        }


        public override void Atualizar(BaseModelo item)
        {
            var pessoa = item as PessoaAvisarModelo;

            Célula = pessoa.Célula;
            IdChatTelegram = pessoa.IdChatTelegram;
        }
    }
}