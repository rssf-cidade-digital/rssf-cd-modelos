﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Modelos.Bases;

namespace Modelos.Modelos
{
    [DataContract]
    public class CélulaModelo : BaseModelo
    {
        [DataMember]
        [Required]
        public string EndereçoMac { get; set; }
        [DataMember]
        [Required]
        public double Latitude { get; set; }
        [DataMember]
        [Required]
        public double Longitude { get; set; }
        [DataMember]
        [Required]
        public string Endereço { get; set; }

        public CélulaModelo()
        {

        }

        public CélulaModelo(string endereçoMac, double latitude, double longitude, string endereço)
        {
            EndereçoMac = endereçoMac;
            Latitude = latitude;
            Longitude = longitude;
            Endereço = endereço;
        }

        public override void Atualizar(BaseModelo item)
        {
            var célula = item as CélulaModelo;

            if (!String.IsNullOrEmpty(célula.EndereçoMac))
            {
                EndereçoMac = célula.EndereçoMac;
            }

            if (!(célula.Latitude == 0))
            {
                Latitude = célula.Latitude;
            }

            if (!(célula.Longitude == 0))
            {
                Longitude = célula.Longitude;
            }

            if (!String.IsNullOrEmpty(célula.Endereço))
            {
                Endereço = célula.Endereço;
            }

        }

        public double CalcularDistância(double latitude, double longitude)
        {
            var dla = (latitude - Latitude) * 60 * 1.852; 
            var dlo = (longitude - Longitude) * 60 * 1.852;

            return Math.Sqrt(Math.Pow(dla, 2) + Math.Pow(dlo, 2));
        }
    }
}
