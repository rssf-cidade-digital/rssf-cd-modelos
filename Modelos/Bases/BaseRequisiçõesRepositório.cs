using Modelos.Modelos;

namespace Modelos.Bases
{
    public abstract class BaseRequisiçõesRepositório: BaseRepositório<RequisiçãoModelo>
    {
        public BaseRequisiçõesRepositório(AplicaçãoContext contexto) : base(contexto)
        {
        }
    }
}