﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Modelos.Bases
{
    public abstract class BaseModelo
    {
        [Required]
        public int Id { get; set; }

        public abstract void Atualizar(BaseModelo item);
    }
}
