using Modelos.Modelos;

namespace Modelos.Bases
{
    public abstract class BaseMonitoramentosRepositório : BaseRepositório<MonitoramentoModelo>
    {
        public BaseMonitoramentosRepositório(AplicaçãoContext contexto) : base(contexto)
        {
        }
    }
}