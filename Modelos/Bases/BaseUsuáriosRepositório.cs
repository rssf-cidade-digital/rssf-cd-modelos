using Modelos.Modelos;
using Modelos.ModelosExibição;

namespace Modelos.Bases
{
    public abstract class BaseUsuáriosRepositório: BaseRepositório<UsuárioModelo>
    {
        public BaseUsuáriosRepositório(AplicaçãoContext contexto) : base(contexto)
        {
        }
        public abstract UsuárioModeloExibição Logar(LoginModeloExibição login);
    }
}