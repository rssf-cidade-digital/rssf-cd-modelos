using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Modelos.Modelos;

namespace Modelos.Bases
{
    public abstract class BaseAlertasRepositório : BaseRepositório<AlertaModelo>
    {
        public BaseAlertasRepositório(AplicaçãoContext contexto) : base(contexto)
        {
        }
    }
}