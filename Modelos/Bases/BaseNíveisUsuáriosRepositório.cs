using Modelos.Modelos;

namespace Modelos.Bases
{
    public abstract class BaseNíveisUsuáriosRepositório: BaseRepositório<NívelUsuárioModelo>
    {
        public BaseNíveisUsuáriosRepositório(AplicaçãoContext contexto) : base(contexto)
        {
        }
    }
}