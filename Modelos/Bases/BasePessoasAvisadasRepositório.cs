using Modelos.Modelos;

namespace Modelos.Bases
{
    public abstract class BasePessoasAvisadasRepositório : BaseRepositório<PessoaAvisadaModelo>
    {
        public BasePessoasAvisadasRepositório(AplicaçãoContext contexto) : base(contexto)
        {
        }
    }
}