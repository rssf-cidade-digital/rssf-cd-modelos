﻿using Microsoft.EntityFrameworkCore;
using Modelos.Interfaces;
using Modelos.Modelos;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Modelos.Bases
{
    public class BaseRepositório<T> : IBaseRepositório<T> where T: BaseModelo 
    {
        protected AplicaçãoContext _contexto;
        protected DbSet<T> _baseDeDados;

        public BaseRepositório(AplicaçãoContext contexto)
        {
            _contexto = contexto;
            _baseDeDados = _contexto.Set<T>();
        }

        public virtual bool InserirVários(List<T> itens)
        {
            foreach (var item in itens)
            {
                InserirItem(item);
            }

            return _contexto.SaveChanges() == itens.Count;
        }

        public virtual bool InserirUm(T item)
        {
            InserirItem(item);

            return _contexto.SaveChanges() == 1;
        }

        public virtual bool Modificar(int id, T registro)
        {
            var item = RetornarPeloId(id);

            if(item == null)
            {
                return false;
            }

            item.Atualizar(registro);
            return _contexto.SaveChanges() == 1;
        }

        public virtual List<T> RetornarLista(Expression<Func<T, bool>> filtro = null)
        {
            if(filtro == null)
            {
                return _baseDeDados.ToList();
            }

            return _baseDeDados.Where(filtro).ToList();
        }

        public virtual T RetornarPeloId(int id)
        {
            return _baseDeDados.Where(k => k.Id == id).SingleOrDefault();
        }

        protected virtual void InserirItem(T item)
        {
            if (item == null)
            {
                throw new NullReferenceException($"{nameof(item)} não pode ser nulo.");
            }

            _baseDeDados.Add(item);
        }

        public virtual bool Remover(int id)
        {
            var item = RetornarPeloId(id);

            if(item == null)
            {
                return false;
            }

            _baseDeDados.Remove(item);
            return _contexto.SaveChanges() == 1;
        }

        public virtual T RetornarÚnico(Expression<Func<T, bool>> filtro)
        {
            return _baseDeDados.Where(filtro).SingleOrDefault();
        }

        public virtual T RetornarÚltimo(Expression<Func<T, bool>> filtro = null, bool ordenarPeloId = false)
        {
            if(filtro == null)
            {
                if(ordenarPeloId)
                {
                    return _baseDeDados.OrderBy(obj => obj.Id).LastOrDefault();
                }

                return _baseDeDados.LastOrDefault();
            }

            if(ordenarPeloId)
            {
                return _baseDeDados.Where(filtro).AsEnumerable().OrderBy(obj => obj.Id).LastOrDefault();
            }

            return _baseDeDados.Where(filtro).AsEnumerable().LastOrDefault();
        }
    }
}
