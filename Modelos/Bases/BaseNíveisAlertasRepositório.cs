using Modelos.Modelos;

namespace Modelos.Bases
{
    public abstract class BaseNíveisAlertasRepositório : BaseRepositório<NívelAlertaModelo>
    {
        public BaseNíveisAlertasRepositório(AplicaçãoContext contexto) : base(contexto)
        {
        }
    }
}