using Modelos.Modelos;

namespace Modelos.Bases
{
    public abstract class BaseCélulasRepositório : BaseRepositório<CélulaModelo>
    {
        public BaseCélulasRepositório(AplicaçãoContext contexto) : base(contexto)
        {
        }
    }
}