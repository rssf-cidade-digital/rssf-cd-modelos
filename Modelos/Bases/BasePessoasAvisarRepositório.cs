using Modelos.Modelos;

namespace Modelos.Bases
{
    public abstract class BasePessoasAvisarRepositório : BaseRepositório<PessoaAvisarModelo>
    {
        public BasePessoasAvisarRepositório(AplicaçãoContext contexto) : base(contexto)
        {
        }
    }
}