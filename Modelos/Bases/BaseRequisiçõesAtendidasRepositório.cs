using Modelos.Modelos;

namespace Modelos.Bases
{
    public abstract class BaseRequisiçõesAtendidasRepositório: BaseRepositório<RequisiçãoAtendidaModelo>
    {
        public BaseRequisiçõesAtendidasRepositório(AplicaçãoContext contexto) : base(contexto)
        {
        }
    }
}